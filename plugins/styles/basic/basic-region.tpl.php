<?php
  drupal_add_css(drupal_get_path('module','panels_overlay_styles') .'/plugins/styles/basic/basic.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));
?>

<div class="basic-region">
  <?php print render($content->content); ?>
</div>