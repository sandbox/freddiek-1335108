<?php
  drupal_add_css(drupal_get_path('module','panels_overlay_styles') .'/plugins/styles/basic/basic.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));

?>

<button class="panels-overlay-trigger" value="#<?php print $pane_id; ?>"><?php
  if(isset($content->title)){
    print $content->title;
  }
  else{
    print t('Open');
  }
?></button>
<div id="<?php print $pane_id; ?>" class="panels-overlay-basic-pane">
  <div class="inner">
    <?php if (isset($content->title)): ?>
    <h2 class="pane-title <?php print (isset($settings['top_color'])) ? $settings['top_color'] : 'blue'; ?>"><?php print $content->title; ?></h2>
    <?php endif ?>
    <div class="pane-content">
    <?php print render($content->content); ?>
    </div>
  </div>
</div>