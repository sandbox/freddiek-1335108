<?php
/**
* Implementation of hook_panels_styles().
*/
$plugin =  array(
  'basic' => array(
    'title' => t('Basic'),
    'description'   => t('basic description'),
    'render pane' => 'basic_render_pane',
    'render region' => 'basic_render_region',
    'pane settings form' => 'basic_settings_form',
    'hook theme'    => array(
      'basic_theme_pane' => array(
        'template' => 'basic-pane',
         'path' => drupal_get_path('module', 'panels_overlay_styles') .'/plugins/styles/basic',
         'variables' => array(  
           'content' => NULL,
           'settings' => NULL,
         ),
       ),
      'basic_theme_region' => array(
        'template' => 'basic-region',
         'path' => drupal_get_path('module', 'panels_overlay_styles') .'/plugins/styles/basic',
         'variables' => array(
           'content' => NULL,
         ),
       ),
    ),
  ),
);

function theme_basic_render_pane($vars) {
    $settings = $vars['settings'];
    $content = $vars['content'];
    $pane_id = "panels-overlay-basic-" . $vars['pane']->subtype . "-" . $vars['pane']->pid;

  return theme('basic_theme_pane', array('content' => $content, 'settings' => $settings, 'pane_id' => $pane_id));
}
function theme_basic_render_region($vars) {
    $content = '';

  foreach ($vars['panes'] as $pane_id => $pane_output) {

    $content .= $pane_output;
  }
  if (empty($content)) {
    return;
  }
  return theme('basic_theme_region', array('content' => $content));
}

/**
 *
 * Anropa menu_link_save() när det hela submittas...
 *
 * @param type $style_settings
 * @return type
 */
function basic_settings_form($style_settings, $display, $pid, $type, $form_state){

 /*$options = menu_parent_options(menu_get_menus(), 'main-menu');
  // If no possible parent menu items were found, there is nothing to display.
  if (empty($options)) {
    return;
  }

  $form['menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu settings'),
    '#access' => user_access('administer menu'),
    '#collapsible' => TRUE,
    '#collapsed' => !$link['link_title'], // if menu item is set, set collapsed to true...
    '#group' => 'additional_settings',
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'taxonomy_menu_form') . '/menu.js'),
    ),
    '#tree' => TRUE,
    '#weight' => -2,
    '#attributes' => array('class' => array('menu-link-form')),
  );
  $form['menu']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide a menu link'),
    '#default_value' => (int) (bool) $link['mlid'], //if the terms has any menu item attached to it. set 1 else 0
  );
  $form['menu']['link'] = array(
    '#type' => 'container',
    '#parents' => array('menu'),
    '#states' => array(
      'invisible' => array(
        'input[name="menu[enabled]"]' => array('checked' => FALSE),
      ),
    ),
  );

  // Populate the element with the link data.
  foreach (array('mlid', 'module', 'hidden', 'has_children', 'customized', 'options', 'expanded', 'hidden', 'parent_depth_limit') as $key) {
    $form['menu']['link'][$key] = array('#type' => 'value', '#value' => $link[$key]);
  }

  $form['menu']['link']['link_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Menu link title'),
    '#default_value' => $link['link_title'],
  );

  $form['menu']['link']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($link['options']['attributes']['title']) ? $link['options']['attributes']['title'] : '',
    '#rows' => 1,
    '#description' => t('Shown when hovering over the menu link.'),
  );

  $default = ($link['mlid'] ? $link['menu_name'] . ':' . $link['plid'] : variable_get('menu_parent_' . $vocabulary->machine_name, 'main-menu:0'));

  if (!isset($options[$default])) {
    $default = 'navigation:0';
  }

  $form['menu']['link']['parent'] = array(
    '#type' => 'select',
    '#title' => t('Parent item'),
    '#default_value' => $default,
    '#options' => $options,
    '#attributes' => array('class' => array('menu-parent-select')),
  );
  $form['menu']['link']['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => $link['weight'],
    '#description' => t('Menu links with smaller weights are displayed before links with larger weights.'),
  );

  // Make sure that our own submit handler is run first before taxonomys own.
  $form['#submit'] = array('basic_settings_form_submit');
  //$form['menu']['#process'] = array('ctools_dependent_process');
  //$form['#process'] = array('basic_settings_form_submit');
*/
  return $form;
}