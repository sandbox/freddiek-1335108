/**
 * Custom event "open-popup" defined, and bound to elements that has the class
 * ".panels-overlay-trigger" and a value that is an ID on the form
 * "#panels-overlay-basic-" and functions as the anchor for positioning and
 * to trigger the panel-pane with the corresponding ID.
 */

(function($){

  $(function(){

    // Example trigger for custom event
    $(".panels-overlay-trigger").bind('click', function(){
      $(this).trigger('open-popup');
    });

    // Open the popup when event is triggered
    $(".panels-overlay-trigger").bind('open-popup', function(){

      var paneId = $(this).attr('value');

      $(".panels-overlay-basic-pane:not(" + paneId + ")").css({'display': 'none'});

/*
      $(paneId).css({
        'top': $(this).offset().top + 25,
        'left': $(this).offset().left
      }); */

      $(paneId).css({
        'top': $(this).position().top + 25,
        'left': $(this).position().left
      });

      $(paneId).toggle();

      event.stopPropagation();
      // event.cancelBubble = true; must be set for IE probably
    });

    //Hide the popup when user clicks outside of popup
    $('html').bind('click', function() {
      $(".panels-overlay-basic-pane").css({'display': 'none'});
    });

    //Prevent popup from closing when clicking inside popup
    $(".panels-overlay-basic-pane").bind('click', function(){
      event.stopPropagation();
      // event.cancelBubble = true; must be set for IE probably
    });

  });

})(jQuery);